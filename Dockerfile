FROM golang:1.16.3-alpine3.13 AS builder
RUN apk --no-cache add ca-certificates make git && update-ca-certificates

COPY . /release-cli
WORKDIR /release-cli

RUN make build

FROM alpine:3.13

RUN adduser -S releaser --uid 1001 -G root
USER releaser

COPY --from=builder --chown=releaser:0 /release-cli/bin/release-cli /usr/local/bin/release-cli
COPY --from=builder --chown=releaser:0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
